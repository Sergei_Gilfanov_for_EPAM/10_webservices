package com.epam.javatraining2016.webservices;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.epam.javatraining2016.webservices.protocol.dataxml.airport.NewDataSet;
import com.epam.javatraining2016.webservices.protocol.jaxws.airport.Airport;
import com.epam.javatraining2016.webservices.protocol.jaxws.airport.AirportSoap;

public class Main {
  static SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
  static URL schemaFile = Main.class.getResource("/schema/AirportData.xsd");

	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException, JAXBException {
		Airport service = new Airport();
		AirportSoap airport = service.getAirportSoap();
		String airportsDataAsString = airport.getAirportInformationByISOCountryCode("ru");
		
		JAXBContext context = JAXBContext.newInstance(NewDataSet.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		
		Schema schema = sf.newSchema(schemaFile);
		unmarshaller.setSchema(schema);
	    unmarshaller.setEventHandler(new ValidationEventHandler() {
	      public boolean handleEvent(ValidationEvent ve) {
	        System.err.println(ve.getLinkedException());
	        return false;
	      }
	    });
	    
	    InputSource is = new InputSource(new StringReader(airportsDataAsString));
		NewDataSet airportsData = (NewDataSet) unmarshaller.unmarshal(is);
		System.out.println(airportsData.getTable().get(0).getAirportCode());
	}

}
